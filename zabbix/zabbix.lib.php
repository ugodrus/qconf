<?php
/**
 * Created by PhpStorm.
 * User: V. Bulychev ( U-GOD/rus/ )
 * Date: 18.02.2019
 * Time: 11:02
 */
require_once ('zabbix.conf.php');

/**
 * Class ApiParams
 */
class ApiParams extends ArrayObject implements JsonSerializable {

	/**
	 * ApiParams constructor.
	 * @param array $input
	 * @param int $flags
	 * @param string $iterator
	 */
	public function __construct($input = [], $flags = 3 , $iterator = "ArrayIterator"){
		parent::__construct([], $flags, $iterator);
		foreach ( $input as $name => $value ){
			if(is_array($value)){
				$this->$name = new self($value);
			} else {
				$this->$name = $value;
			}
		}
	}

	/**
	 * @return array
	 */
	public function jsonSerialize() {
		return $this->getArrayCopy();
	}

	/**
	 * @param array $input
	 * @param int $flags
	 * @param string $iterator
	 * @return ApiParams
	 */
	public static function make($input = [], $flags = 3 , $iterator = "ArrayIterator"){
		return new static($input, $flags, $iterator);
	}

	/**
	 * @param $name
	 * @param $value
	 * @return ApiParams
	 */
	public function set($name, $value){
		if(is_array($value)){
			$this->$name = new self($value);
		} else {
			$this->$name = $value;
		}
		return $this;
	}

	/**
	 * @param $name
	 * @return mixed
	 */
	public function get($name){
		return $this->$name;
	}

	/**
	 * @return array
	 */
	public function getArrayCopy(){
		$copy = parent::getArrayCopy();
		foreach($copy as $key => $value ){
			if(is_object($value) && $value instanceof self){
				$copy[$key] = $value->getArrayCopy();
			}
		}
		return $copy;
	}

	/**
	 * @return array
	 */
	public function toArray(){
		return $this->getArrayCopy();
	}
}

/**
 * Class ZabbixApiRequest
 */
class ZabbixApiRequest {

	/**
	 * @var string
	 */
	protected $method = '';

	/**
	 * @var string
	 */
	protected $rpcver = '2.0';

	/**
	 * @var ApiParams
	 */
	protected $params;

	/**
	 * @var string
	 */
	protected $zabbixurl = '';

	/**
	 * @var string
	 */
	protected $zabbixusr = '';

	/**
	 * @var string
	 */
	protected $zabbixpwd = '';

	/**
	 * @var string
	 */
	protected $zabbixid = '';

	/**
	 * @var string
	 */
	protected $zabbixauth = '';

	/**
	 * @var string
	 */
	protected $zabbixmeth = '';

	/**
	 * @var ZabbixApiResponse
	 */
	protected $result;

	/**
	 * ZabbixApiRequest constructor.
	 * @param string $method
	 */
	public function __construct($method = 'apiinfo.version')
	{
		$this->method;
		$this->params = ApiParams::make();
	}

	/**
	 * @param string $method
	 * @return ZabbixApiRequest
	 */
	public static function make($method = 'apiinfo.version')
	{
		return new static($method);
	}

	/**
	 * @param ZabbixApiRequest $request
	 * @return ZabbixApiRequest
	 */
	public static function makeFromRequest(ZabbixApiRequest $request)
	{
		$newRequest = new static('apiinfo.version');
		$newRequest->fillFromRequest($request);
		return $newRequest;
	}

	/**
	 * @return ZabbixApiRequest
	 */
	public function run()
	{
		$this->result = ZabbixApiResponse::make();


		$curl = curl_init();
		curl_setopt_array(
			$curl,
			[
				CURLOPT_POST => true,
				CURLOPT_URL => $this->getServer(),
				CURLOPT_POSTFIELDS => json_encode( $this->composeRequest() ),
				CURLOPT_RETURNTRANSFER => false,
				CURLOPT_VERBOSE => false,
				CURLOPT_HEADER => false,
				CURLOPT_HTTPHEADER => [
					'Content-Type: application/json-rpc'
				],
				CURLOPT_HEADERFUNCTION => [$this->result,'appendHeader'],
				CURLOPT_WRITEFUNCTION => [$this->result,'appendBody']
			]
		);

		curl_exec($curl);

		$this->result->setOptions(curl_getinfo($curl))->prepare();
		try {
			$this->result->throwErrorIfExists();
			$this->preparseResult();
		} catch (Exception $exception){
			// do nothing
		}
		curl_close($curl);

		return $this;
	}

	/**
	 *
	 */
	protected function preparseResult(){
		if(
			strtolower($this->method) == 'user.login' &&
			$this->result->getBody()->result
		){
			$this->zabbixauth = $this->result->getBody()->result;
			$this->zabbixid = $this->result->getBody()->id;
		}
	}

	/**
	 * @return array
	 */
	protected function composeRequest(){

		$query = [
			'jsonrpc' => $this->rpcver,
			'method' => strtolower($this->method),
			'params' => [],
			'id' => $this->zabbixid,
			'auth' => $this->zabbixauth,
		];

		switch (strtolower($this->method)){

			case 'user.login':
				$query['params'] = [
					'user' => $this->zabbixusr,
					'password' => $this->zabbixpwd
				];
				$query['id'] = 1;
				$query['auth'] = null;
				break;
			case 'apiinfo.version':
				$query['params'] = [];
				$query['id'] = 1;
				$query['auth'] = null;
				break;
			default:
				$query['params'] = $this->params->getArrayCopy();
		}

		return $query;

	}

	/**
	 * @param $method
	 * @return ZabbixApiRequest
	 */
	public function setMethod($method)
	{
		$this->method = $method;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getMethod()
	{
		return $this->method;
	}

	/**
	 * @param $user
	 * @return ZabbixApiRequest
	 */
	public function setUser($user)
	{
		$this->zabbixusr = $user;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getUser()
	{
		return $this->zabbixusr;
	}

	/**
	 * @param $password
	 * @return ZabbixApiRequest
	 */
	public function setPassword($password)
	{
		$this->zabbixpwd = $password;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->zabbixpwd;
	}

	/**
	 * @param $url
	 * @return ZabbixApiRequest
	 */
	public function setServer($url)
	{
		$this->zabbixurl = $url;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getServer()
	{
		return $this->zabbixurl;
	}

	/**
	 * @param $id
	 * @return ZabbixApiRequest
	 */
	public function setId($id)
	{
		$this->zabbixid = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->zabbixid;
	}

	/**
	 * @param $auth
	 * @return ZabbixApiRequest
	 */
	public function setAuth($auth)
	{
		$this->zabbixauth = $auth;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAuth()
	{
		return $this->zabbixauth;
	}

	/**
	 * @param ApiParams $params
	 * @return ZabbixApiRequest
	 */
	public function setParams(ApiParams $params)
	{
		$this->params = $params;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getParams()
	{
		return $this->params->getArrayCopy();
	}

	/**
	 * @return ZabbixApiRequest
	 */
	public function clearParams(){
		$this->params = ApiParams::make();
		return $this;
	}

	/**
	 * @param ZabbixApiRequest $request
	 * @return ZabbixApiRequest
	 */
	public function fillFromRequest(ZabbixApiRequest $request){
		$this->setServer($request->getServer());
		$this->setMethod($request->getMethod());
		$this->setUser($request->getUser());
		$this->setPassword($request->getPassword());
		$this->setId($request->getId());
		$this->setAuth($request->getAuth());
		$this->params = ApiParams::make($request->getParams());
		return $this;
	}

	/**
	 * @return ZabbixApiResponse
	 */
	public function getResult()
	{
		return $this->result;
	}

	/**
	 *
	 */
	public function dump(){
		var_dump($this);
	}
}

/**
 * Class ZabbixApiResponse
 */
class ZabbixApiResponse {

	/**
	 * @var array
	 */
	protected $headers = [];

	/**
	 * @var array
	 */
	protected $options = [];

	/**
	 * @var string
	 */
	protected $body = '';

	/**
	 * @var bool|ApiParams
	 */
	protected $bodyarr = false;

	/**
	 * @var bool
	 */
	protected $locked = false;

	/**
	 * ZabbixApiResponse constructor.
	 */
	public function __construct(){}

	/**
	 * @return ZabbixApiResponse
	 */
	public static function make(){
		return new static();
	}

	/**
	 * @return ZabbixApiResponse
	 */
	public function prepare(){
		$this->locked = true;
		$barrnew = json_decode($this->body, true);
		$this->bodyarr = ApiParams::make(
			($barrnew && is_array($barrnew) && sizeof($barrnew))? $barrnew : []
		);
		return $this;
	}

	/**
	 * @param $curl
	 * @param $header
	 * @return int
	 */
	public function appendHeader($curl, $header){
		if(
			!is_resource($curl) ||
			get_resource_type($curl) != 'curl' ||
			$this->locked
		){
			return strlen($header);
		}

		$this->headers[] = $header;
		return strlen($header);
	}

	/**
	 * @param $curl
	 * @param $body
	 * @return int
	 */
	public function appendBody($curl, $body){
		if(
			!is_resource($curl) ||
			get_resource_type($curl) != 'curl' ||
			$this->locked
		){
			return strlen($body);
		}

		$this->body .= $body;
		return strlen($body);
	}

	/**
	 * @param $options
	 * @return ZabbixApiResponse
	 */
	public function setOptions($options)
	{
		if($this->locked){ return $this; }
		$this->options = $options;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * @return array
	 */
	public function getHeaders()
	{
		return $this->headers;
	}

	/**
	 * @return bool|string|ApiParams
	 */
	public function getBody()
	{
		return ($this->bodyarr && $this->bodyarr instanceof ApiParams)? $this->bodyarr : $this->body;
	}

	/**
	 *
	 */
	public function dump(){
		var_dump($this);
	}

	/**
	 * @return ZabbixApiResponse
	 * @throws Exception
	 * @throws ZabbixApiException
	 */
	public function throwErrorIfExists(){
		if(
			!$this->bodyarr ||
			!$this->bodyarr instanceof ApiParams
		){
			throw new Exception('Unparsed responce data', 500);
		} elseif (
			$this->bodyarr &&
			@$this->bodyarr->error &&
			$this->bodyarr->error instanceof ApiParams &&
			$this->bodyarr->error->count()
		){
			$Error = $this->bodyarr->error;
			throw new ZabbixApiException($Error->message, $Error->data, $Error->code);
		}
		return $this;
	}
}

/**
 * Class ZabbixApiException
 */
class ZabbixApiException extends Exception {

	/**
	 * @var string
	 */
	protected $data;

	/**
	 * ZabbixApiException constructor.
	 * @param string $message
	 * @param string $data
	 * @param int $code
	 * @param Throwable|NULL $previous
	 */
	public function __construct ($message = "", $data = "", $code = 0 , $previous = NULL ){
		parent::__construct($message, $code, $previous);
		$this->data = $data;
	}

	/**
	 * @return string
	 */
	public function getData()
	{
		return $this->data;
	}
}
