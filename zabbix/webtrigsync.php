<?php
/**
 * Created by PhpStorm.
 * User: V. Bulychev ( U-GOD/rus/ )
 * Date: 20.02.2019
 * Time: 9:54
 */
require_once ('zabbix.lib.php');

try {

	$loginRequest = ZabbixApiRequest::make()
		->setServer(ZAB_SERVER)
		->setMethod('user.login')
		->setUser(ZAB_USER)
		->setPassword(ZAB_PASS)
		->run();

	$loginRequest->getResult()->throwErrorIfExists();

} catch (Exception $e){
	print $e->getMessage();
	exit(0);
}

try {
	$getUnusedRequest = ZabbixApiRequest::makeFromRequest($loginRequest)
		->setMethod('item.get')
		->clearParams()
		->setParams(
			ApiParams::make([
				'output' => ['itemid', 'type', 'hostid', 'name', 'key_'],
				'filter' => ['type' => 9],
				'search' => ['key_' => 'web.test.fail'],
				'webitems' => true,
				'with_triggers' => false,
				'selectHosts' => ['hostid', 'host', 'name']
			])
		)
		->run();

	$getUnusedRequest->getResult()->throwErrorIfExists();

	if ($getUnusedRequest->getResult()->getBody()->result->count()) {

		$InsertNew = [];

		foreach ($getUnusedRequest->getResult()->getBody()->result as $Item) {
			array_push(
				$InsertNew,
				[
					'expression' => sprintf("{%1\$s:%2\$s.max(#3)}>0", $Item->hosts[0]->host, $Item->key_),
					'description' => preg_replace('/([^[]+\[)([^[]+)(\])/', '[SITE] $2 unavailable', $Item->key_),
					'status' => 0,
					'priority' => 5
				]
			);
		}

		$addUnusedRequest = ZabbixApiRequest::makeFromRequest($loginRequest)
			->setMethod('trigger.create')
			->clearParams()
			->setParams(
				ApiParams::make(
					$InsertNew
				)
			)
			->run();

		$addUnusedRequest->getResult()->throwErrorIfExists();

		if (sizeof($addUnusedRequest->getResult()->getBody()->result->triggerids) != sizeof($InsertNew)) {
			printf(
				"%s of %s triggers created",
				sizeof($addUnusedRequest->getResult()->getBody()->result->triggerids),
				sizeof($InsertNew)
			);
		}

		print $addUnusedRequest->getResult()->getBody()->result->triggerids->count().' triggers created';

	}

} catch (Exception $e){
	print $e->getMessage();
}

$logoutRequest = ZabbixApiRequest::makeFromRequest($loginRequest)
	->setMethod('user.logout')
	->clearParams()
	->setParams(ApiParams::make())
	->run();

print 'OK';