#!/bin/bash

cat << EOF
# ServerName gives the name and port that the server uses to identify itself.
# This can often be determined automatically, but we recommend you specify
# it explicitly to prevent problems during startup.
# If your host doesn't have a registered DNS name, enter its IP address here.
EOF
read -p 'ServerName : ' servername

cat << EOF

# The ServerAlias directive sets the alternate names for a host, for use with
# name-based virtual hosts. The ServerAlias may include wildcards, if
# appropriate.
# NOTE ! *.${servername} will be added by default
EOF
read -p 'ServerAlias : ' serveralias

cat << EOF

# ServerAdmin: Your address, where problems with the server should be
# e-mailed.  This address appears on some server-generated pages, such
# as error documents.  e.g. admin@your-domain.com
# NOTE keep empty to default : admin@${servername}
EOF
read -p 'ServerAdmin : ' serveradmin
serveradmin="${serveradmin:=admin@${servername}}"

cat << EOF

CHECK YOUR RESULT CONFIG:

EOF

eval "echo \"$(cat /etc/httpd/conf.vhosts.d/vhost.conf.tpl)\""

cat << EOF

Do you want create this host?
EOF

read -p 'Type y/n : ' confirm

if echo "$confirm" | grep -iq "^y"; then
   mkdir -p -m 0775 /var/www/domains/${servername}
   chown -R apache:apache /var/www/domains/${servername}
   eval "echo \"$(cat /etc/httpd/conf.vhosts.d/vhost.conf.tpl)\"" > /etc/httpd/conf.vhosts.d/${servername}.conf
fi